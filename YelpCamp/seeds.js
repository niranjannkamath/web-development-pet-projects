var moongoose = require("mongoose"),
    Campground = require("./models/campground"),
    Comment = require("./models/comment");

var data = [
    {
        name: "Clouds Rest",
        image: "https://unsplash.com/photos/ELevCx8PX4o",
        description: "blah blah blah"
    },
    {
        name: "Desert Mesa",
        image: "https://unsplash.com/photos/tvicgTdh7Fg",
        description: "blah blah blah"
    },
    {
        name: "Canyon Floor",
        image: "https://unsplash.com/photos/uyE5g_fdM54",
        description: "blah blah blah"
    }
];

function seedDB() {
    Campground.remove({}, function(err) {
        if(err) {
            console.log(err);
        }
        else {
            console.log("removed campgrounds");
        }
        data.forEach(function(seed) {
            Campground.create(seed, function(err, campground) {
                if(err) {
                    console.log(err);
                }
                else {
                    console.log("added a campground");
                    Comment.create(
                        {
                            text: "This place is great, but I wish there was Interet.",
                            author: "Homer"
                        }, function(err, comment) {
                            if(err) {
                                console.log(err);
                            }
                            else {
                                campground.comments.push(comment);
                                campground.save();
                                console.log("created new comment");
                            }
                        });
                }
            });
        });
    }); 
}
    
module.exports = seedDB;