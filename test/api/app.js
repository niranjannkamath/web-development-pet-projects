const app = require("express")();
const cors = require("cors");
const bodyParser = require("body-parser");

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var disabledButtons = [false, false];

//routes
app.get("/buttons", function(req, res) {
    res.send({disabledButtons: disabledButtons});
});

app.post("/buttons", function(req, res) {
    var data = req.body;
    disabledButtons[Number(data.clicked)] = true;
});

app.post("/reset", function(req, res) {
    var buttonState = req.body.buttonState;
    disabledButtons = buttonState;
});

//server listen
const port = process.env.PORT || 9000;
app.listen(port, function() {
    console.log("Server Started");
});