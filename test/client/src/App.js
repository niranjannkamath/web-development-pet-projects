import React from "react";
import {Switch, Route} from "react-router-dom";
import LoginForm from "./login/login";
import Buttons from "./buttons/buttons";

class App extends React.Component {
    // componentDidMount() {
    //     fetch("http://localhost:9000/buttons", {
    //         method: "GET",
    //         mode: "cors",
    //         headers: {
    //             "Access-Control-Allow-Origin": "*",
    //             "Content-Type": "application/json"
    //         }
    //     })
    //     .then(res => res.json())
    //     .then(data => this.setState({disabledButtons: data.disabledButtons}))
    //     .catch(error => console.log(error.message))
    // }

    // handleSubmit(event) {
    //     event.preventDefault();
    //     var data = new FormData(event.target);
    //     console.log(data.get("username"));
    //     fetch("http://localhost:9000/auth", {
    //         method: "POST",
    //         redirect: "manual",
    //         mode: "cors",
    //         referrer: "",
    //         referrerPolicy: "no-referrer",
    //         headers: {
    //             "Access-Control-Allow-Origin": "Content-Type",
    //             "Content-Type": ["multipart/form-data", "application/json"]
    //         },
    //         body: data
    //     })
    //     .then(res => this.setState({redirect: true}));
    // }

    render() {
        return (
            <Switch>
                <Route path="/buttons"><Buttons /></Route>
                <Route path="/"><LoginForm /></Route>
            </Switch>
        )
    }
}

export default App;