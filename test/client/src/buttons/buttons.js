import React from "react";
import "firebase/firestore";
import firebase from "../config";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import "./buttons.css";

var db = firebase.firestore();

var btns = db.collection("buttons");

class Buttons extends React.Component {
    constructor(props) {
        super(props);
        this.state = {numButtons: 0, disabledButtons: []};
        this.handleClick = this.handleClick.bind(this);
        this.updateState = this.updateState.bind(this);
        this.resetDB = this.resetDB.bind(this);
        //make db listen to modification queries
        btns.onSnapshot(snapshot => {
            snapshot.docChanges().forEach(change => {
                if(change.type === "modified") {
                    this.updateState(change.doc.ref);
                }
            });
        });
    }

    componentDidMount() {
        //initialise button state from server side data
        fetch("http://localhost:9000/buttons", {
            method: "GET",
            mode: "cors",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
        .then(res => res.json())
        .then(data => this.setState({numButtons: data.disabledButtons.length, disabledButtons: data.disabledButtons}))
        .catch(error => console.log(error.message));
    }

    componentWillUnmount() {
        this.resetDB();
    }

    //reset buttons to enabled state
    resetDB() {
        var buttonState = [];
        for(var i = 1; i <= this.state.numButtons; i++) {
            var id = "b" + i.toString();
            btns.doc(id).update({clicked: false})
                        .then(() => console.log("Button Reset"))
                        .catch(error => console.log(error.message)); 
            
            buttonState.push(false);
        }

        var data = {};
        data["buttonState"] = buttonState;

        fetch("http://localhost:9000/reset", {
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
        .catch(error => console.log(error.message));
    }

    //update button state when db document modified
    updateState(docRef) {
        var disabled = this.state.disabledButtons;
        disabled[Number(docRef.id.slice(1)) - 1] = !disabled[Number(docRef.id.slice(1)) - 1];
        this.setState({disabledButtons: disabled});
    }

    //change button state on click
    handleClick(event) {
        event.preventDefault();
        var updatedBtnRef = btns.doc(event.target.id);
        updatedBtnRef.update({clicked: true})
            .then(() => console.log("State Updated"))
            .catch(error => console.log(error.message));
        var data = {}
        data["clicked"] = event.target.value;
        fetch("http://localhost:9000/buttons", {
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
        .catch(error => console.log(error.message));
    }

    render() {
    let buttons = this.state.disabledButtons.map((state, i) => <Button variant="outline-primary"
                                                                        key={i.toString()}
                                                                        id={"b" + (i + 1).toString()}
                                                                        value={i} disabled={state}
                                                                        onClick={this.handleClick}>
                                                                            B{i + 1}
                                                                        </Button>)
        return (
            <Container>
                <div className="btn-container">
                    {buttons}
                </div>
                <div className="reset-btn">
                    <Button variant="outline-primary" onClick={this.resetDB}>Reset Database</Button>
                </div>
                <Form.Text className="text-muted">
                    Click Me before using this app and to reset at any point
                </Form.Text>
            </Container>
        )
    }
}

export default Buttons;