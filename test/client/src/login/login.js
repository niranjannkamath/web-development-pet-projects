import React from "react";
import { Redirect } from "react-router-dom";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import "./login.css";
import "firebase/auth";
import firebase from "../config";

var auth = firebase.auth();

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {username: '', password: '', redirect: false};

        this.submitUsername = this.submitUsername.bind(this);
        this.submitPassword = this.submitPassword.bind(this);
        this.submit = this.submit.bind(this)
    }

    //update username asuser types
    submitUsername(event) {
        this.setState({username: event.target.value});
    }

    //update password as user types
    submitPassword(event) {
        this.setState({password: event.target.value});
    }

    //submit form
    submit(event) {
        event.preventDefault();
        auth.signInWithEmailAndPassword(event.target.username.value, event.target.password.value)
            .then(() => this.setState({redirect: true}))
            .catch(error => {
                alert(error.message);
            });
    }

    render() {
        if(this.state.redirect) {  //redirect to buttons page if login successful
            return <Redirect to="/buttons" />
        }
        return(
            <Container>
                <Form onSubmit={this.submit}>
                    <Form.Group>
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="email" value={this.state.username} name="username" onChange={this.submitUsername} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={this.state.password} name="password" onChange={this.submitPassword} />
                    </Form.Group>
                    <Button variant="outline-primary" type="submit">Login</Button>
                </Form> 
            </Container>
        )
    }
}

export default LoginForm;