import * as firebase from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyCtFvB8bqxaX3d1bAstt-DkBGcwetiBaQQ",
    authDomain: "testsite-12a9d.firebaseapp.com",
    databaseURL: "https://testsite-12a9d.firebaseio.com",
    projectId: "testsite-12a9d",
    storageBucket: "testsite-12a9d.appspot.com",
    messagingSenderId: "169930123733",
    appId: "1:169930123733:web:0ed437aefac0be0d16f55c",
    measurementId: "G-STNF0YH0SH"
};

firebase.initializeApp(firebaseConfig);

export default firebase;